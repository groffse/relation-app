This Assignment was completed by:
Bjarte Isachsen
Gunnar Sigstad
Håkon Underdal
Andreas Klophmann


All CRUD operations is available in the "Dao" package, 
but aren't called from the main() method for now.

When you run the main() method you will be prompted to search for a name.
There are just a few names in the database-file for now, 
but you can search for one of our first names or your own. 
All other data in the database-file is made up.

After the name search you will be prompted to search for a phone number.
Some of the numbers in the Database-file are: 90909090, 80808080, 70707070 etc.