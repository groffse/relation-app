package com.company.Models;

public class RelationshipType {
    private String type;

    public RelationshipType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
