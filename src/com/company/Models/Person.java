package com.company.Models;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private int id;
    private String firstName;
    private String lastName;
    private int phoneWork;
    private int phoneHome;
    private int phoneMobile;
    private String eMail;
    private String mailWork;
    private Address address = null;
    private String dateOfBirth;
    private List<Relationship> relationships= new ArrayList<Relationship>();

    public Person (){}

    public Person (String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(int id, String firstName, String lastName, int phoneWork, int phoneHome, int phoneMobile, String eMail, String mailWork, String streetName, int streetNr, int postCode, String city, String dateOfBirth) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneWork = phoneWork;
        this.phoneHome = phoneHome;
        this.phoneMobile = phoneMobile;
        this.eMail = eMail;
        this.mailWork = mailWork;
        this.address = new Address(streetName, streetNr, postCode, city);
        this.dateOfBirth = dateOfBirth;
    }

    public Person(String firstName, String lastName, int phoneWork, int phoneHome, int phoneMobile, String eMail, String mailWork, String streetName, int streetNr, int postCode, String city, String dateOfBirth) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneWork = phoneWork;
        this.phoneHome = phoneHome;
        this.phoneMobile = phoneMobile;
        this.eMail = eMail;
        this.mailWork = mailWork;
        this.address = new Address(streetName, streetNr, postCode, city);
        this.dateOfBirth = dateOfBirth;
    }
    public Person(String firstName, String lastName, int phoneWork, int phoneHome, int phoneMobile, String eMail, String mailWork,  String streetName, int streetNr, int postCode, String city, String dateOfBirth, ArrayList<Relationship> relationships) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneWork = phoneWork;
        this.phoneHome = phoneHome;
        this.phoneMobile = phoneMobile;
        this.eMail = eMail;
        this.mailWork = mailWork;
        this.address = new Address(streetName, streetNr, postCode, city);
        this.dateOfBirth = dateOfBirth;
        this.relationships = relationships;
    }

    // Setters
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneWork(int phoneWork) {
        this.phoneWork = phoneWork;
    }

    public void setPhoneHome(int phoneHome) {
        this.phoneHome = phoneHome;
    }

    public void setPhoneMobile(int phoneMobile) {
        this.phoneMobile = phoneMobile;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public void setMailWork(String mailWork) { this.mailWork = mailWork; }

    public void setAddress(String streetName, int streetNr, int postCode, String city) {
        this.address = new Address(streetName, streetNr, postCode, city);
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    public void setId(int id) {this.id = id;}


    // Getters
    public int getId() { return id; }
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getPhoneWork() {
        return phoneWork;
    }

    public int getPhoneHome() {
        return phoneHome;
    }

    public int getPhoneMobile() {
        return phoneMobile;
    }

    public String geteMail() {
        return eMail;
    }

    public String getMailWork() { return mailWork; }

    public Address getAddress() {
        return address;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }
}
