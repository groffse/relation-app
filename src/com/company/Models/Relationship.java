package com.company.Models;

public class Relationship {
    private String person2;
    private int relationId;

    public Relationship(String person2, int relationId) {
        this.person2 = person2;
        this.relationId = relationId;
    }

    public String getPerson2() {
        return person2;
    }

    public void setPerson2(String person2) {
        this.person2 = person2;
    }
}
