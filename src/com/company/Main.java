package com.company;

import com.company.Dao.SelectDao;
import com.company.Dao.PostDao;
import com.company.Models.Address;
import com.company.Models.Person;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static ArrayList<Person> people = new ArrayList<Person>();
    public static ArrayList<Address> addresses = new ArrayList<Address>();

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Search for a name:");
        String search = input.next();

        printPeople(search);
        printPersonAndRelations(search);

        System.out.println("Search for a phone nr:");
        int phone = input.nextInt();
        input.close();

        printPerson(phone);
        /*Person person = new Person(
                "Dewald",
                "Dewaldsen",
                40404040,
                40505051,
                40505052,
                "dewaldsen@testmail.no",
                "dewaldsen@workmail.no",
                "Vearveien",
                72,
                3137,
                "Vear",
                "05.11.1978");
        // new PostDao().addPerson(person);
        new PostDao().addRelationship(7,6, 1);*/
    }

    public static void printPeople(String name){
        new SelectDao().selectPeople(name);
        for (Person person : people ){
            System.out.print(person.getFirstName() + ", ");
            System.out.print(person.getLastName() + ", ");
            System.out.print(person.getAddress().getStreetName() + " ");
            System.out.print(person.getAddress().getStreetNumber() + ", ");
            System.out.print(person.getAddress().getPostalCode() + " ");
            System.out.print(person.getAddress().getCity() + ", ");
            System.out.print(person.getPhoneHome() + ", ");
            System.out.print(person.getPhoneWork() + ", ");
            System.out.print(person.getPhoneMobile() + ", ");
            System.out.print(person.geteMail() + "\n");
        }
    }

    public static void printPerson(int nr){
        Person person = new SelectDao().selectPerson(nr);

        System.out.print("Person with phone nr " + nr + ": " + person.getFirstName() + ", ");
        System.out.print(person.getLastName() + ", ");
        System.out.print(person.getAddress().getStreetName() + " ");
        System.out.print(person.getAddress().getStreetNumber() + ", ");
        System.out.print(person.getAddress().getPostalCode() + " ");
        System.out.print(person.getAddress().getCity() + ", ");
        System.out.print(person.getPhoneHome() + ", ");
        System.out.print(person.getPhoneWork() + ", ");
        System.out.print(person.getPhoneMobile() + ", ");
        System.out.print(person.geteMail() + "\n");

    }

    public static void printPersonAndRelations(String name){
        Person person = new SelectDao().selectPerson(name);
        System.out.print("The relationships of ");
        System.out.print(person.getFirstName() + " ");
        System.out.print(person.getLastName() + " are: \n");
        new SelectDao().selectRelations(person.getId());

    }
}
