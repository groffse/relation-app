package com.company.Dao;

import com.company.ConnectDB;
import com.company.Models.Address;
import com.company.Models.Person;
import com.company.Models.Post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PostDao {
    private ConnectDB db;

    public PostDao() {
        db = new ConnectDB();
    }

    public void addPerson(Person person) {
        int addressId = 0;

        // check if person got address
        if (person.getAddress() != null) {
            Post post = new SelectDao().selectPost(person.getAddress().getPostalCode());

            // check if postCode exists in db
            if (post == null) {

                // if no postCode in db, insert both postcode and address

                addPostCode(person.getAddress().getPostalCode(), person.getAddress().getCity());

                // call addAddress method
                addAddress(
                        person.getAddress().getStreetName(),
                        person.getAddress().getStreetNumber(),
                        person.getAddress().getPostalCode());
            }

            // if postCode exists in db check if address exists
            else {
                Address address = new SelectDao().selectAddress(
                        person.getAddress().getStreetName(),
                        person.getAddress().getStreetNumber(),
                        person.getAddress().getPostalCode());
                if (address == null) {
                    addAddress(
                            person.getAddress().getStreetName(),
                            person.getAddress().getStreetNumber(),
                            person.getAddress().getPostalCode());
                }
            }
            if(addressId == 0) {
                Address address = new SelectDao().selectAddress(
                        person.getAddress().getStreetName(),
                        person.getAddress().getStreetNumber(),
                        person.getAddress().getPostalCode());
                addressId = address.getId();
            }

            String personSql = "INSERT INTO Person (FirstName, LastName, PhoneHome, PhoneMobile, PhoneWork, Mail, MailWork, AddressId, DateOfBirth) " +
                    "VALUES (?,?,?,?,?,?,?,?,?)";

            try (Connection conn = db.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(personSql)) {

                stmt.setString(1, person.getFirstName());
                stmt.setString(2, person.getLastName());
                stmt.setInt(3, person.getPhoneHome());
                stmt.setInt(4, person.getPhoneMobile());
                stmt.setInt(5, person.getPhoneWork());
                stmt.setString(6, person.geteMail());
                stmt.setString(7, person.getMailWork());
                stmt.setInt(8, addressId);
                stmt.setString(9, person.getDateOfBirth());
                stmt.execute();

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

        } else {
            String personSql = "INSERT INTO Person (FirstName, LastName, PhoneHome, PhoneMobile, PhoneWork, Mail, MailWork, DateOfBirth) " +
                    "VALUES (?,?,?,?,?,?,?,?)";

            try (Connection conn = db.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(personSql)) {

                stmt.setString(1, person.getFirstName());
                stmt.setString(2, person.getLastName());
                stmt.setInt(3, person.getPhoneHome());
                stmt.setInt(4, person.getPhoneMobile());
                stmt.setInt(5, person.getPhoneWork());
                stmt.setString(6, person.geteMail());
                stmt.setString(7, person.getMailWork());
                stmt.setString(8, person.getDateOfBirth());
                stmt.execute();

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void addPostCode(int postCode, String city){
        String postSql = "INSERT INTO Post (ID, City) VALUES (?,?);";

        try {
            Connection conn = db.getConnection();
            PreparedStatement stmt = conn.prepareStatement(postSql);
            stmt.setInt(1, postCode);
            stmt.setString(2, city);
            stmt.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    public void addAddress (String street, int number, int postcode) {
        String addressSql = "INSERT INTO Address (Street, Number, PostCode) VALUES (?,?,?);";

        try {
            Connection conn = db.getConnection();
            PreparedStatement stmt = conn.prepareStatement(addressSql);
            stmt.setString(1, street);
            stmt.setInt(2, number);
            stmt.setInt(3, postcode);
            stmt.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addRelationship(int pId1, int pId2, int relationId){
        String relationSql = "INSERT INTO Relationship (Person_1, Person_2, RelationId) VALUES (?,?,?);";

        try{
            Connection conn = db.getConnection();
            PreparedStatement stmt = conn.prepareStatement(relationSql);
            stmt.setInt(1, pId1);
            stmt.setInt(2, pId2);
            stmt.setInt(3, relationId);

            stmt.execute();
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
}

