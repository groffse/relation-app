package com.company.Dao;

import com.company.ConnectDB;
import com.company.ConnectDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//TODO: Don't make a function for each table. Let one function handle all by taking a table name as parameter

public class UpdateDao {
    private ConnectDB db;
    private Connection conn;

    public UpdateDao() {
        db = new ConnectDB();
        conn = db.getConnection();
    }

    public void update(String tableName, String ID, String ... values) {

        // Storing values
        HashMap<String, String> fields_values = new HashMap<>();
        ArrayList<String> types = new ArrayList<>();
        for(String value : values) {
            String temp[] = value.split(":");
            fields_values.put(temp[0], temp[1]);
            types.add(temp[2]);
        }

        // Put this in it's own function
        String update_query =  "UPDATE " + tableName +"\n" +
                                "SET ";

        Iterator it = fields_values.entrySet().iterator();
        int typeCounter = 0;
        while(it.hasNext()) {

            Map.Entry<String, String> pair = (Map.Entry)it.next();
            update_query = update_query + pair.getKey() + " = " + get_value(types.get(typeCounter++), pair.getValue()) + ", ";
        }
        update_query = update_query.substring(0, update_query.length() - 2); //Remove last comma and space
        update_query += "\n"
                        + "WHERE ID = " + ID;

        try {
            PreparedStatement statement = this.conn.prepareStatement(update_query);
            statement.executeUpdate();
        }catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    // Checks the type of the column and returns the value in proper form
    private String get_value (String type, String value) {
        String val = value;
       if(type.equals("Text")) {
           val = "\'" + value + "\'";
       }
       return val;
    }

}
