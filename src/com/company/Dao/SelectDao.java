package com.company.Dao;
import com.company.ConnectDB;
import com.company.Main;
import com.company.Models.Address;
import com.company.Models.Person;
import com.company.Models.Post;
import com.company.Models.Relationship;

import java.sql.*;


public class SelectDao {
    private ConnectDB db;

    public SelectDao(){
        db = new ConnectDB();

    }

    public Person selectPerson(int nr){
        String sql = "SELECT p.*, a.*, Post.City " +
                "FROM Person p, Address a, Post " +
                "WHERE (p.PhoneHome = ? OR p.PhoneMobile = ? OR p.PhoneWork = ?) " +
                "AND p.AddressID = a.ID AND a.PostCode = Post.ID;";
        Person person = null;
        try(Connection conn = db.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql)){

            stmt.setInt(1, nr);
            stmt.setInt(2, nr);
            stmt.setInt(3, nr);
            ResultSet rs = stmt.executeQuery();

            Post post = new Post(
                    rs.getInt("ID"),
                    rs.getString("City")
            );
            Address address = new Address(
                    rs.getString("Street"),
                    rs.getInt("Number"),
                    rs.getInt("PostCode"),
                    post.getCity()
            );
            String adr = address.getStreetName()+" "+address.getStreetNumber()+" "+address.getPostalCode()+" "+address.getCity();
            person = new Person(
                    rs.getInt("ID"),
                    rs.getString("FirstName"),
                    rs.getString("LastName"),
                    rs.getInt("PhoneWork"),
                    rs.getInt("PhoneHome"),
                    rs.getInt("PhoneMobile"),
                    rs.getString("Mail"),
                    rs.getString("MailWork"),
                    rs.getString("Street"),
                    rs.getInt("Number"),
                    rs.getInt("PostCode"),
                    rs.getString("City"),
                    rs.getString("DateOfBirth")
            );

        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return person;
    }

    public Person selectPerson(String name){
        String sql = "SELECT p.*, a.*, Post.City " +
                "FROM Person p, Address a, Post " +
                "WHERE (p.FirstName LIKE ? OR p.LastName LIKE ?) " +
                "AND p.AddressID = a.ID AND a.PostCode = Post.ID;";
        Person person = null;
        try(Connection conn = db.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql)){

            stmt.setString(1, '%'+name+'%');
            stmt.setString(2,'%'+name+'%');
            ResultSet rs = stmt.executeQuery();

            Post post = new Post(
                    rs.getInt("ID"),
                    rs.getString("City")
            );
            Address address = new Address(
                    rs.getString("Street"),
                    rs.getInt("Number"),
                    rs.getInt("PostCode"),
                    post.getCity()
            );
            String adr = address.getStreetName()+" "+address.getStreetNumber()+" "+address.getPostalCode()+" "+address.getCity();
            person = new Person(
                    rs.getInt("ID"),
                    rs.getString("FirstName"),
                    rs.getString("LastName"),
                    rs.getInt("PhoneWork"),
                    rs.getInt("PhoneHome"),
                    rs.getInt("PhoneMobile"),
                    rs.getString("Mail"),
                    rs.getString("MailWork"),
                    rs.getString("Street"),
                    rs.getInt("Number"),
                    rs.getInt("PostCode"),
                    rs.getString("City"),
                    rs.getString("DateOfBirth")
            );

        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return person;
    }

    public void selectPeople(String name){
        String sql = "SELECT p.*, a.*, Post.City " +
                "FROM Person p, Address a, Post " +
                "WHERE (p.FirstName LIKE ? OR p.LastName LIKE ?) " +
                "AND p.AddressID = a.ID AND a.PostCode = Post.ID;";
        Person person = null;
        try(Connection conn = db.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql)){

            stmt.setString(1, '%'+name+'%');
            stmt.setString(2,'%'+name+'%');
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){

                Post post = new Post(
                        rs.getInt("ID"),
                        rs.getString("City")
                );
                Address address = new Address(
                        rs.getString("Street"),
                        rs.getInt("Number"),
                        rs.getInt("PostCode"),
                        post.getCity()
                );
                String adr = address.getStreetName()+" "+address.getStreetNumber()+" "+address.getPostalCode()+" "+address.getCity();
                person = new Person(
                        rs.getInt("ID"),
                        rs.getString("FirstName"),
                        rs.getString("LastName"),
                        rs.getInt("PhoneWork"),
                        rs.getInt("PhoneHome"),
                        rs.getInt("PhoneMobile"),
                        rs.getString("Mail"),
                        rs.getString("MailWork"),
                        rs.getString("Street"),
                        rs.getInt("Number"),
                        rs.getInt("PostCode"),
                        rs.getString("City"),
                        rs.getString("DateOfBirth")

                );
                Main.people.add(person);
            }

        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        ;
    }


    public void selectRelations(int id){
        String sql = "SELECT p2.FirstName, p2.LastName, RelationshipType.Type " +
                "FROM Person p1, Person p2 " +
                "INNER JOIN Relationship " +
                "ON p1.ID = Relationship.Person_1 AND p2.ID = Relationship.Person_2 " +
                "INNER JOIN RelationshipType " +
                "ON Relationship.RelationId = RelationshipType.ID " +
                "WHERE p1.ID = ?";
        try(Connection conn = db.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql)){

            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()){
                System.out.print(rs.getString("FirstName") + " ");
                System.out.print(rs.getString("LastName") + " ");
                System.out.print(rs.getString("Type") + "\n");
            }
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public Post selectPost(int postNr){
        String sql = "SELECT ID, City FROM Post WHERE ID = ?";
        Post post = null;
        try(Connection conn = db.getConnection();
            PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setInt(1, postNr);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                post = new Post(rs.getInt("ID"), rs.getString("City"));
            }
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return post;
    }

    public Address selectAddress(String street, int number, int postNr ){
        String addressSql = "SELECT * FROM Address WHERE Street = ? AND Number = ? AND PostCode = ?";
        Address address = null;
        try (Connection conn = db.getConnection();
             PreparedStatement stmt = conn.prepareStatement(addressSql)) {

            stmt.setString(1, street);
            stmt.setInt(2, number);
            stmt.setInt(3, postNr);

            ResultSet rs = stmt.executeQuery();
            address = new Address(
                    rs.getInt("ID"),
                    rs.getString("Street"),
                    rs.getInt("Number"),
                    rs.getInt("PostCode"));
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return address;
    }
}
