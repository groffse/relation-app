package com.company.Dao;

import com.company.ConnectDB;
import com.company.Main;
import com.company.Models.Address;
import com.company.Models.Person;
import com.company.Models.Post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DeleteDao {

    private ConnectDB db;
    private Connection conn;

    public DeleteDao(){
        db = new ConnectDB();
        conn = db.getConnection();
    }

    public void deletePerson(int id) {
        String sql = "DELETE FROM Person WHERE ID = ?";

        try {
            PreparedStatement stmt = this.conn.prepareStatement(sql);

            stmt.setInt(1,id);
            int success = stmt.executeUpdate();

            if (success == 1) {
                String sql2 = "DELETE FROM Relationship WHERE " +
                        "Relationship.Person_1 = ? OR Relationship.Person_2 = ?";
                PreparedStatement stmt2 = this.conn.prepareStatement(sql2);
                stmt2.setInt(1,id);
                stmt2.setInt(2,id);
                int success2 = stmt2.executeUpdate();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteAddress(int id) {
        String sql = "DELETE FROM Address WHERE ID = ?";

        try {
            PreparedStatement stmt = this.conn.prepareStatement(sql);

            stmt.setInt(1,id);
            int success = stmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteRelationship(int id) {
        String sql = "DELETE FROM Relationship WHERE ID = ?";

        try {
            PreparedStatement stmt = this.conn.prepareStatement(sql);

            stmt.setInt(1,id);
            int success = stmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deletePost(int id) {
        String sql = "DELETE FROM Post WHERE ID = ?";

        try {
            PreparedStatement stmt = this.conn.prepareStatement(sql);

            stmt.setInt(1,id);
            int success = stmt.executeUpdate();

            if (success == 1) {
                String sql2 = "DELETE FROM Address WHERE " +
                        "PostCode = ?";
                PreparedStatement stmt2 = this.conn.prepareStatement(sql2);
                stmt2.setInt(1,id);
                int success2 = stmt2.executeUpdate();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
